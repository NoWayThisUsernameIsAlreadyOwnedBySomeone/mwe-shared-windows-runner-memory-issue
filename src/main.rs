fn main() {
    let resp = reqwest::blocking::get("https://httpbin.org/get").unwrap();

    println!("{}", resp.text().unwrap());
}
